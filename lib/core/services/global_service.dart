import 'package:flutter/widgets.dart';

class GlobalService {
  GlobalService._apiConstructor();

  static final GlobalService _instance = GlobalService._apiConstructor();

  factory GlobalService() {
    return _instance;
  }

  final GlobalKey<NavigatorState> _navigator = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigator => _navigator;
  BuildContext get context => _navigator.currentContext;
}
