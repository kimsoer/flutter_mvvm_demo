import 'package:flutter_mvvm_demo/core/util/config.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../enums/local_service.dart';
import '../repositories/api_repository.dart';
import 'local_service.dart';

class ApiProviderService {
  ApiProviderService._apiConstructor();

  static final ApiProviderService _instance =
      ApiProviderService._apiConstructor();

  factory ApiProviderService() {
    return _instance;
  }

  Dio _dio = Dio(
    BaseOptions(
      connectTimeout: 5000,
      sendTimeout: 5000,
      receiveTimeout: 5000,
      baseUrl: baseApiUrl,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    ),
  );

  Dio get getDioInstance => _dio;

  RestClient getRestClient() {
    return RestClient(ApiProviderService().getDioInstance);
  }

  getInstance() {
    if (!kReleaseMode) {
      _dio.interceptors.add(
        PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
        ),
      );
    }
  }

  setUserAccessToken(String accessToken) {
    LocalService().saveValue(LocalDataFieldName.USER_TOKEN, accessToken);
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $accessToken",
    };
  }

  clearUserAccessToken() {
    LocalService().deleteSavedValue(LocalDataFieldName.USER_TOKEN);
    _dio.options.headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };
  }
}
