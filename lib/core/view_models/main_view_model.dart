


import 'package:flutter_mvvm_demo/core/models/responses/post_response_entity.dart';
import 'package:flutter_mvvm_demo/core/services/api_provider_service.dart';
import 'package:stacked/stacked.dart';

class MainViewModel extends BaseViewModel{


  List<PostResponseEntity> dataList = [];
  getInstance(){
    clearErrors();
    setBusy(false);
    notifyListeners();
    getPostData();
    setInitialised(true);
    notifyListeners();
  }

  getPostData() async{
    try{
      setBusy(true);
      notifyListeners();

      var response = await ApiProviderService().getRestClient().getPostData();
      // print("response=> ${response.data}");
      for(var data in response.data){
       dataList.add(PostResponseEntity().fromJson(data));
      }

    }catch(e){
      print("error=> $e");
    }finally{
      setBusy(false);
      notifyListeners();
    }
  }

}