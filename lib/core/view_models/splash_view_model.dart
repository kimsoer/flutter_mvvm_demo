
import 'package:flutter_mvvm_demo/core/enums/screen_type.dart';
import 'package:flutter_mvvm_demo/core/services/api_provider_service.dart';
import 'package:flutter_mvvm_demo/ui/views/main_view.dart';
import 'package:stacked/stacked.dart';

import '../global_function.dart';
import '../services/local_service.dart';

class SplashViewModel extends BaseViewModel {
  getInstance() async {
    ApiProviderService().getInstance();
    await LocalService().getInstance();

    Future.delayed(Duration(seconds: 2), () {
      startNewScreen(ScreenType.SCREEN_WIDGET_REPLACEMENT, MainView());
    });
  }
}
