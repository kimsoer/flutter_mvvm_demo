
import 'package:flutter_mvvm_demo/core/util/config.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'api_repository.g.dart';

@RestApi(baseUrl: baseApiUrl)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("posts")
  Future<HttpResponse> getPostData();
}
