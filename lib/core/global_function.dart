import 'dart:async';
import 'package:flutter_mvvm_demo/core/enums/screen_type.dart';
import 'package:flutter_mvvm_demo/core/util/constants.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';
import 'package:stacked/stacked.dart';

import 'services/api_provider_service.dart';
import 'services/global_service.dart';

class GlobalFunction {
  ///Handle log printing in debug build
  static printDebugMessage(data) {
    if (!kReleaseMode) {
      if (data.toString().length < 2000) {
        Logger().d(data);
      }
    }
  }

  ///On http request fail handler
  static onHttpRequestFail(dynamic error, BaseViewModel model) {
    try {
      if (error is DioError) {
        if (error?.response?.statusCode == 419 ||
            error?.response?.statusCode == 401) {
          ApiProviderService().clearUserAccessToken();
          Navigator.of(GlobalService().context)
              .popUntil((route) => route.isFirst);

          //TODO: PUSH REPLACEMENT TO YOUR FIRST PAGE AND SHOW ALERT MESSAGE TO USER

        } else if (error.type == DioErrorType.connectTimeout ||
            error.type == DioErrorType.receiveTimeout ||
            error.type == DioErrorType.sendTimeout ||
            error?.response?.data == null) {
          model.setError("មិនអាចភ្ជាប់ទៅម៉ាស៊ីនមេទេ");
        } else {
          model.setError(
              error?.response?.data["message"] ?? "មិនអាចភ្ជាប់ទៅម៉ាស៊ីនមេទេ");
        }
      } else {
        model.setError("មានអ្វីមួយមិនប្រក្រតី... សូមព្យាយាមម្តងទៀត...");
      }
    } catch (error) {
      model.setError("មានអ្វីមួយមិនប្រក្រតី... សូមព្យាយាមម្តងទៀត...");
    }
  }
}

startNewScreen(ScreenType screenType, Widget widget) {
  if (screenType == ScreenType.SCREEN_WIDGET_REPLACEMENT) {
    GlobalService().navigator.currentState.pushReplacement(
          MaterialPageRoute(
            builder: (context) => widget,
          ),
        );
  } else {
    GlobalService().navigator.currentState.push(
          MaterialPageRoute(
            builder: (context) => widget,
          ),
        );
  }
}

startNewScreenAnimation(ScreenType screenType, Widget widget) {
  if (screenType == ScreenType.SCREEN_WIDGET_REPLACEMENT) {
    Navigator.of(GlobalService().context).pushReplacement(PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => widget,
      transitionDuration: Duration(milliseconds: 700),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    ));
  } else {
    Navigator.of(GlobalService().context).push(PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => widget,
      transitionDuration: Duration(milliseconds: 700),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    ));
  }
}

flutterToast(String message) {
  Timer _timer;
  return showDialog<void>(
    context: GlobalService().context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      _timer = Timer(Duration(seconds: 1), () {
        Navigator.of(context).pop();
      });
      return Container(
        alignment: Alignment.center,
        child: Material(
          color: Colors.black54,
          borderRadius: BorderRadius.circular(20),
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
            child: Text(
              message,
              style: theme.bodyText2.copyWith(color: Colors.white),
            ),
          ),
        ),
      );
    },
  ).then((value) {
    if (_timer.isActive) {
      _timer.cancel();
    }
  });
}
