import 'package:flutter_mvvm_demo/generated/json/base/json_convert_content.dart';

class PostResponseEntity with JsonConvert<PostResponseEntity> {
	int userId;
	int id;
	String title;
	String body;
}
