import 'package:flutter/material.dart';

const Color PRIMARY_COLOR = Color.fromARGB(0xFF, 0, 131, 72);
const Color SECONDARY_COLOR = Color.fromARGB(0xFF, 149, 193, 31);
const Color ACCENT_COLOR = Color.fromARGB(0xFF, 255, 204, 0);
const Color DISABLE_COLOR = Color.fromARGB(0xFF, 200, 200, 211);
