


import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_mvvm_demo/core/view_models/main_view_model.dart';
import 'package:flutter_mvvm_demo/ui/widgets/view_model_state_handler_widget.dart';
import 'package:stacked/stacked.dart';

class MainView extends ViewModelBuilderWidget<MainViewModel>{



  @override
  void onViewModelReady(MainViewModel viewModel) {
    viewModel.getInstance();
  }

  @override
  MainViewModel viewModelBuilder(BuildContext context) {
    return MainViewModel();
  }
  @override
  Widget builder(BuildContext context, MainViewModel viewModel, Widget child) {
   return ViewModelStateHandlerWidget<MainViewModel>(child: Scaffold(
       appBar: AppBar(
         title: Text("Home"),
       ),
       body: ListView.builder(
           itemCount: viewModel.dataList.length,
           itemBuilder: (context, index){
             var data = viewModel.dataList[index];
             return Card(
               child: Text("${data.body}"),
             );
           })
   ));
  }

}
