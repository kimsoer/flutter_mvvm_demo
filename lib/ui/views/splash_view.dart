
import 'package:flutter_mvvm_demo/core/util/colors.dart';
import 'package:flutter_mvvm_demo/core/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../core/view_models/splash_view_model.dart';

class SplashView extends ViewModelBuilderWidget<SplashViewModel> {
  @override
  SplashViewModel viewModelBuilder(BuildContext context) {
    return SplashViewModel();
  }

  @override
  void onViewModelReady(SplashViewModel model) {
    model.getInstance();
  }

  @override
  Widget builder(
      BuildContext context, SplashViewModel viewModel, Widget child) {
    return Scaffold(
      backgroundColor: PRIMARY_COLOR,
      body: Container(
        width: size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircularProgressIndicator(
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
